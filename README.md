# Dig Deeper

![IMAGE_DESCRIPTION](https://i.imgur.com/2OhYnTZ.png)


This is an OpenSource game based in Godot. 

You play as a miner avoiding zombies by digging deeper into the cave system, blocking your entrance with barriers. The goal is to survive till a certain time (currently 1min 30sec).

This game demos how to use the AStarGrid2D object in Godot and using auto tiles for the most part.

I have many plans to improve this in my free time and I think this could be a very cool game. It is loosely based on some of the zombie custom games made in WC3 reign of chaos

## Features

- [x] Enemy pathfinding
- [X] Fog of war
- [ ] multiple ores
- [ ] Skeleton enemy
- [ ] intro cut scene
- [ ] upgrading barriers
- [ ] Settings menu
- [ ] Tile animations
- [ ] unique spawn points

## Gameplay:
![IMAGE_DESCRIPTION](https://i.imgur.com/Z85YqP1.png)
![IMAGE_DESCRIPTION](https://i.imgur.com/yxrdGZw.png)
