extends CharacterBody2D

@export var speed:float = 20
@export var damage:float = 1
@export var attackSpeed:float = 1

@onready var animation_player = $Sprite2D/AnimationPlayer
@onready var sprite_2d = $Sprite2D


var levelManager:Node2D
@export var player:Node2D
var map:TileMap

# status
var oldPos:Vector2 = Vector2.INF
var mining:bool = false
var nearBarriers = []

signal reacedBarrier
signal navigationFinished

# navigation
var path = [] 

func _ready():
	levelManager = get_node('/root/Node2D')

	if (not levelManager.is_node_ready()):
		await levelManager.ready
	
	map = levelManager.tile_map
	player = levelManager.player
	
	map.barrier_removed.connect(_on_barrier_removed)
	$Timer.start()


func _physics_process(_delta):
	HandleAnimation()

	var dir = to_local(get_new_path_position()).normalized()
	velocity = dir * speed 
	move_and_slide()
	
func makePath(pos :Vector2):
	var result = []
	for point in map.find_path(self, player):
		result.append(map.map_to_local(point+map.grid_offset))
	
	map.is_player_reachable = result.size()>0
	return result
	
func get_new_path_position():
	if(path.size() < 1):
		return global_position
	if(position.distance_to(path[0]) < 7):
		path.remove_at(0)
		if(path.size() < 1):
			emit_signal("navigationFinished")
			makePath(player.position)
			return global_position
	return path[0]
	
func Mine(cell: Vector2i):
	var barrier:StaticBody2D
	if map.barriers.has(cell) :
		barrier= map.barriers[cell]
	else:
		mining = false
		return
	var oldspeed = speed
	speed = 0
	mining = true
	
	while (barrier != null &&  barrier.HP > 0):
		barrier.TakeDamage(damage)
		await get_tree().create_timer(attackSpeed).timeout

	speed = oldspeed
	mining = false
	
func _on_timer_timeout():
	
	if (mining):
		return
	if(player == null):
		printerr("Enemy has not found player!")
		return
	
	var p = makePath(player.global_position)
	if (not map.is_player_reachable):
		print("no good path to player")
		map.SetNavBarriers(false)
	
	path = p  
	if(oldPos.distance_to(position) < $Timer.wait_time/(speed)):
		queue_free()
	oldPos = position
	$Timer.wait_time = remap(position.distance_to(player.position),0,1000,.2,4)
	


func NearbyCells(cell:Vector2i):
	var result = []
	for dx in [-1, 0, 1]:
		for dy in [-1, 0, 1]:
			var new_x = cell.x + dx
			var new_y = cell.y + dy
			if not(new_x == 0 && new_y == 0):
				var newCell = Vector2i(new_x,new_y)
				var tile:TileData = map.get_cell_tile_data(1,newCell)
				if(tile != null):
#					print("near a barrier")
					result.append(newCell)
	return result


func HandleAnimation():
	if(velocity == Vector2.ZERO):
		animation_player.stop()
		return
	var anim = ""
	
	if(velocity.y < 0):
		anim = "WalkUp"
	elif(velocity.y > 0):
		anim = "WalkDown"
	if(abs(velocity.x) > abs(velocity.y)):
		if(velocity.x != 0):
			sprite_2d.flip_h = 0 if velocity.x < 0 else 1
			anim = "WalkLeft"
	
	animation_player.play(anim)



func _on_area_2d_body_entered(body:Node2D):
	if(body.is_in_group('Barrier') && map.attack_barriers):
		Mine(map.local_to_map(body.position))
		pass
	pass # Replace with function body.

func _on_barrier_removed():
	map.is_player_reachable = true
	map.SetNavBarriers(true)


## --------------------------old code --------------------------
#var result = []
#	for point in map.find_path(self, player):
#
#		if(map.get_cell_tile_data(1,point) != null):
#			var tile_type = map.get_cell_tile_data(1,point).get_custom_data('tile_type')
#			if (tile_type == 'barrier'):
#				return result
#		result.append(map.map_to_local(point+map.grid_offset))


#var barriers = map.barriers
#	if (barriers.size() < 1):
#		printerr("no barriers to destroy, should be impossible")
#		queue_free()
#		mining = false
#		return

#	if(body.is_in_group("tilemap")):
#		var near = NearbyCells(map.local_to_map(position))
#		#var near = map.local_to_map(body.position)
#		for cell in near:
#			if map.get_cell_tile_data(1,cell) == null:
#				continue
#			var tile_type = map.get_cell_tile_data(1,cell).get_custom_data("tile_type")
#			if  tile_type != 'barrier':
#				continue
#			if nearBarriers.has(cell):
#				continue
#			nearBarriers.append(cell) 
#			print(tile_type)

#func _on_area_2d_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
#
#			Mine(cell)
##			map.RemoveBarrier(cell)
#	pass # Replace with function body.
