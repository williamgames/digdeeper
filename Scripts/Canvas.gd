extends CanvasLayer

@onready var main : Control = $Main
@onready var settings : Control = $Settings




func _on_button_play_pressed():
	print("hit play")
	get_tree().change_scene_to_file("res://Scenes/GameRoom.tscn") 
	pass # Replace with function body.


func _on_button_settings_pressed():
	main.visible = false
	settings.visible = true
	pass # Replace with function body.


func _on_button_quit_pressed():
	get_tree().quit()
	pass # Replace with function body.


func _on_button_settings_back_pressed():
	main.visible = true
	settings.visible = false
	pass # Replace with function body.
