extends CharacterBody2D

@export_category('Player Stats:')
@export var speed: float = 50
@export var mineSpeed:float = 1
@export var sightRange:float =1
@export var barrierLevel:int = 1

@export_category('Other:')
@export var sounds :Dictionary = {
	"Mined":"res://Art/SFX/MineSFX2.mp3",
	"barrier_placed":"res://Art/SFX/PlaceSFX.mp3"
} 

@export var mapPath:NodePath
@onready var map = get_node(mapPath)
@export var itemPath: NodePath
@onready var item:Node2D = get_node(itemPath)
@onready var sprite_2d = $Sprite2D
@onready var animation_player = $Sprite2D/AnimationPlayer
@onready var pickaxe = $Area2D
@onready var audio_stream_player_2d = $AudioStreamPlayer2D

signal gameOver()

# private:
var mouse_left_down:bool = false
var oldAnim = ""
var dir:String = ""
# Called when the node enters the scene tree for the first time.
func _ready():
	if map.is_node_ready():
			
		map.connect('barrier_placed',_on_barrier_placed)
	pass # Replace with function body.

func _unhandled_input( event ):
	if event is InputEventMouseButton:
		if event.button_index == 1 and event.is_pressed():
			mouse_left_down = true
		elif event.button_index == 1 and not event.is_pressed():
			mouse_left_down = false
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	movement(delta)
	for index in get_slide_collision_count():
		var collision := get_slide_collision(index)
		var body :Object= collision.get_collider()
#		print("Collided with:" + body.name)
		if(body.is_in_group("Enemy")):
			emit_signal("gameOver")
	
	item.visible = mouse_left_down
func _physics_process(_delta):
	HandleAnimation()
func movement(_delta):
	var vert:float = 0
	var horz:float = 0
	if(Input.is_action_pressed('up')):
		vert -= 1
	if(Input.is_action_pressed('down')):
		vert += 1
	if(Input.is_action_pressed('left')):
		horz -= 1
	if(Input.is_action_pressed('right')):
		horz +=1
		
	velocity = Vector2(horz,vert) * speed
	move_and_slide()
	
func HandleAnimation():
	
	var action = ""
	
	if(pickaxe.mining):
		action = "Mine"
		animation_player.speed_scale = 1/mineSpeed
	else:
		action = "Walk"
		animation_player.speed_scale = 1
		
		if(velocity == Vector2.ZERO):
			animation_player.stop()
			return
			
	if(velocity.y < 0):
		dir = "Up"
		sprite_2d.flip_h = 0
	elif(velocity.y > 0):
		dir = "Down"
		sprite_2d.flip_h = 0
	
	if(velocity.x != 0):
		sprite_2d.flip_h = 0 if velocity.x < 0 else 1
		dir = "Left"

	animation_player.play(action+dir)
	

func _on_barrier_placed():
	audio_stream_player_2d.stream = load(sounds['barrier_placed'])
	audio_stream_player_2d.play()

func _on_area_2d_mined():
	audio_stream_player_2d.stream = load(sounds['Mined'])
	audio_stream_player_2d.play()
	pass # Replace with function body.
