extends Sprite2D

const LightTexture =  preload("res://Art/GameSprites/Light.png")
const GRID_SIZE = 16

var fog = self
@onready var player = $"../Player"
@onready var tile_map = $"../TileMap"
var display_width:int 
var display_height:int

var fogImage = Image.new()
var fogTexture = ImageTexture.new()
var lightImage:Image = LightTexture.get_image()
var light_offset = Vector2(0,0)
# Called when the node enters the scene tree for the first time.
func _ready():
	self.position = tile_map.get_used_rect().position * GRID_SIZE
	
	display_width = tile_map.get_used_rect().size.x
	display_height = tile_map.get_used_rect().size.y
	
	var fog_image_width = display_width
	var fog_image_height = display_height
	print(fog_image_height)
	fogImage = Image.create(fog_image_width, fog_image_height, false, Image.FORMAT_RGBAH)
	fogImage.fill(Color(0.5,0.5,0.5	,1))
	
	fog.scale *= GRID_SIZE
	lightImage.convert(Image.FORMAT_RGBAH)
	print(lightImage)
	light_offset = Vector2(lightImage.get_width()/2, lightImage.get_height()/2)
	pass # Replace with function body.

func update_fog(new_grid_position):
#	fogImage.lock()
#	lightImage.lock()
	
	var light_rect = Rect2(Vector2.ZERO, Vector2(lightImage.get_width(), lightImage.get_height()))
	fogImage.blend_rect(lightImage, light_rect,new_grid_position - light_offset) #new_grid_position - light_offset)
	
#	fogImage.unlock()
#	lightImage.unlock()
	update_fog_image_texture()

func update_fog_image_texture():
	
	fogTexture = ImageTexture.create_from_image(fogImage)
	fog.texture = fogTexture

func _physics_process(delta):
#	update_fog_image_texture()
	var pos = Vector2(tile_map.local_to_map(player.position)-tile_map.get_used_rect().position)
	print("pos is: " + str(pos))
	update_fog(pos)
