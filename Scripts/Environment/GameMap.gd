extends TileMap

var astar_grid:AStarGrid2D
var barriers = {}
var wallHPs = {}


@export var grid_offset :Vector2i
@export var Barrier:PackedScene
@export var is_player_reachable:bool = true
@export var attack_barriers:bool = false

signal barrier_removed
signal barrier_placed

@onready var node_2d = $Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	self.set_layer_enabled(5,true)
#		self.set_cell(5,i,1,Vector2i(0,23))
#	grid_offset = get_used_rect().size
	
#	for cell in get_used_cells(0):
#		if get_cell_tile_data(3,cell) != null:
#			erase_cell(0,cell)
	
	removeShadow(Vector2i(100,100))
	_init_grid()
	update_grid_from_tilemap()

	astar_grid.default_compute_heuristic = AStarGrid2D.HEURISTIC_EUCLIDEAN
	astar_grid.default_estimate_heuristic = AStarGrid2D.HEURISTIC_EUCLIDEAN
	astar_grid.diagonal_mode = AStarGrid2D.DIAGONAL_MODE_AT_LEAST_ONE_WALKABLE
	astar_grid.jumping_enabled = false
func removeShadow(cell:Vector2i):
	
	var flood = node_2d.flood_fill(cell,100,node_2d.is_occupied)
	
	for i in flood:
#		await get_tree().create_timer(.001).timeout
#		await get_tree().create_timer(100)
		var neigbors = [Vector2i(-1,-1),Vector2i(-1,0),Vector2i(-1,1),Vector2i(0,-1),Vector2i(0,1),Vector2i(1,-1),Vector2i(1,0),Vector2i(1,1)]
		for j in neigbors:
			if(self.get_cell_source_id(3, i+j) >= 0):
				self.set_cell(5,i+j,-1)
		self.set_cell(5,i,-1)
func AttackWall(cell : Vector2i):
	var currentHp = wallHPs[cell]
	print(currentHp)
	
	currentHp -= 1
	if(currentHp <=0):
		RemoveWall(cell)
	else:
		wallHPs[cell] = currentHp

func RemoveWall(cell : Vector2i):
	self.erase_cell(3,cell)
	self.erase_cell(4,cell)
#	self.set_cell(0,cell,1,Vector2i(8,16))
	
	var sur = []
	for c in Globals.nearbyCells(cell):
		if(self.get_cell_tile_data(3,c) != null):
			self.erase_cell(3,c)
			sur.append(c)

	self.set_cells_terrain_connect(3,sur,0,0,true)
	
func AddBarrier(cell:Vector2i):
#	set_cell(0,cell,1,Vector2i(7,16))
#	set_cell(1,cell,1,Vector2i(16,29))
	var b:StaticBody2D = Barrier.instantiate()
	add_child(b)
	b.map = self
	b.position = map_to_local(cell)
	barriers[cell] = b
	astar_grid.set_point_solid(cell-grid_offset, true)
	emit_signal("barrier_placed")
func RemoveBarrier(cell:Vector2i):
	barriers[cell].Destroyed()
	barriers.erase(cell)
	astar_grid.set_point_solid(cell, false)
	emit_signal("barrier_removed")
	
func SetNavBarriers(CanNav:bool):
	attack_barriers = not CanNav
	for cell in barriers:
		astar_grid.set_point_solid(cell-grid_offset, CanNav)

#		set_cell(0,cell,1,AtlasPos)

func _init_grid() -> void:
	astar_grid = AStarGrid2D.new()
	astar_grid.size = self.get_used_rect().size
#	astar_grid.offset = self.get_used_rect().position
	astar_grid.cell_size = self.tile_set.tile_size
	astar_grid.update()
	
func update_grid_from_tilemap() -> void:
	for i in range(astar_grid.size.x):
		for j in range(astar_grid.size.y):
			var id = Vector2i(i, j) + grid_offset
			# If game_map does not have a cell source id >= 0
			# then we're looking at an invalid location
			if(self.get_cell_source_id(0, id) >= 0):
				astar_grid.set_point_solid(Vector2i(i, j), false)
				if(get_cell_atlas_coords(0,id) == Vector2i(3,4)):
					astar_grid.set_point_solid(Vector2i(i, j), true)
			if self.get_cell_source_id(3, id) >= 0:
				astar_grid.set_point_solid(Vector2i(i, j), true)
#		
#				var tile_type = self.get_cell_tile_data(0, id).get_custom_data('tile_type')
#			# If looking at a location outside of the game map,
			# default to marking the cell solid so the player can't navigate
#			# outside of the game map.
#			# Shouldn't be an issue in this demo since grid size comes from map size
#			# but something to keep in mind.
#			else:
#				astar_grid.set_point_solid(Vector2i(i, j), true)

func find_path(start, goal):

	var start_cell = self.local_to_map(start.global_position)
	var end_cell = self.local_to_map(goal.global_position)
	
	var adj_start_cell = start_cell - grid_offset
	var adj_end_cell = end_cell - grid_offset
	if  !astar_grid.is_in_boundsv(adj_start_cell) or !astar_grid.is_in_boundsv(adj_end_cell):
		print(str(adj_start_cell) +" | "+ str(astar_grid.offset))
		print(str(adj_end_cell) +" | "+ str(astar_grid.size))
		printerr("bad input to find_path")
		return
	if start_cell == end_cell:
		return [end_cell]
	var id_path:Array[Vector2i] = astar_grid.get_id_path(adj_start_cell, adj_end_cell)

#	for id in id_path:
#		set_cell(1, id+grid_offset, 1, Vector2(0, 0))
	return id_path
	
