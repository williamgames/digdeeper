extends StaticBody2D

#Stats
@export var HP:float = 3

@export var map:TileMap

# Called when the node enters the scene tree for the first time.
func _ready():
	map =  get_node('..')
#	print ("barrier placed")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func TakeDamage(dmg:float):
	HP -= dmg
	if (HP<= 0):
		map.RemoveBarrier(map.local_to_map(self.position))
func Destroyed():
	queue_free()
