@tool
extends Node2D

@onready var tileMap:TileMap = $".."
@onready var astar_grid = tileMap.astar_grid
@export var my_prop: bool:
	get:
		return false
	set(value):
		
		redraw(value)

const DIRECTIONS = [Vector2.LEFT, Vector2.RIGHT, Vector2.UP, Vector2.DOWN]

func _ready():
	update_grid_from_tilemap()
	pass

func _process(delta):
#	print("test")
	pass
#	position += Vector2.RIGHT * delta
func redraw(value = false):
	
	pass
	# only do this if we are working in the editor
	
#	print((tileMap.get_used_rect().size))
#	var flood = flood_fill(Vector2i(125,188),100)
#
#	for i in flood:
##		tileMap.set_cell(5,i,1,Vector2i(0,23))
#		tileMap.set_cell(5,i,-1)
#	return value
	
	
	
func update_grid_from_tilemap() -> void:
	var count = 0
	for i in range(tileMap.get_used_rect().size.x):
		for j in range(tileMap.get_used_rect().size.y):
			var id = Vector2i(i, j) + tileMap.grid_offset
#			print(tileMap.get_cell_source_id(3, id) )
			# If game_map does not have a cell source id >= 0
			# then we're looking at an invalid location
			
			
			if(tileMap.get_cell_source_id(3, id) >= 0 ):
				
#			if (tileMap.get_cell_atlas_coords(3,id) == Vector2i(9,2)):
				if(randi()%5 == 1):
					tileMap.set_cell(4,id,4,Vector2i(6,0))
					tileMap.wallHPs[id] = 2
				else:
					tileMap.erase_cell(4,id)
					tileMap.wallHPs[id] = 1
			else:
#				tileMap.set_cell(4,id,-1)
#				tileMap.erase_cell(4,id)
#				if(tileMap.get_cell_atlas_coords(0,id) == Vector2i(3,4)):
				count += 1
#			tileMap.set_cell(5,id,1,Vector2i(0,23))
#			if tileMap.get_cell_source_id(3, id) >= 0:
#	print( "wall" + str(count))

func is_occupied(cell : Vector2i) -> bool:
	var wall = tileMap.get_cell_source_id(3, cell) >= 0
	var ground = tileMap.get_cell_source_id(0, cell) >= 0
	
	return (not ground) or wall

func flood_fill(cell: Vector2i, max_distance: int, checkfunc: Callable) -> Array:
	var array := []
	var stack := [cell]
	while not stack.is_empty():
		var current: Vector2i = stack.pop_back()

#		print(tileMap.get_used_rect().has_point(current))
		if not tileMap.get_used_rect().has_point(current):
			continue
		if current in array:
			continue

		var difference: Vector2 = (current - cell).abs()
		var distance := int(difference.x + difference.y)
		if distance > max_distance:
			continue
		array.append(current)
		for direction in DIRECTIONS:
			var coordinates: Vector2 = current + Vector2i (direction)
			if checkfunc.call(coordinates):
				continue
			if coordinates in array:
				continue
			stack.append(coordinates)
	return array
