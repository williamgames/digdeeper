extends Node2D

@export var LevelManager:Node2D
@export var Zombie:PackedScene

var zombies = []
# Called when the node enters the scene tree for the first time.
func _ready():
	await get_tree().create_timer(2).timeout
	$Timer.start()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass



func _on_timer_timeout():
	#Create zombie
	if(zombies.size()> 100):
		return
	var z = Zombie.instantiate()
	LevelManager.add_child(z)
#	z.map = LevelManager.tile_map
#	z.player = LevelManager.player
	z.position = position
	zombies.append(z)
	pass # Replace with function body.
