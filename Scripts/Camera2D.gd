extends Camera2D

@export var player:CharacterBody2D
@export var map:TileMap

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var diff = position - player.position
	position -= diff/10
	
	var min:Vector2 = map.map_to_local( map.get_used_rect().position) + (get_viewport_rect().size/zoom)/2 - Vector2(8,8)
	var max:Vector2 = map.map_to_local( map.get_used_rect().position + map.get_used_rect().size) - (get_viewport_rect().size/zoom)/2 - Vector2(8,8)

	position = position.clamp(min,max)

	pass

