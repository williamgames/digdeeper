extends Node



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
func nearbyCells(cell:Vector2i, dist:int = 1):
	var result = []
	var tiles = []
	
	for dx in range(-dist,dist+1):
		for dy in range(-dist,dist+1):
			var offset = Vector2i(dx,dy)
			var newcell = cell + offset
			tiles.append(newcell)	
	tiles.erase(Vector2i(0,0))
	
	result = tiles
	return result
