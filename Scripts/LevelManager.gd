extends Node2D


@export var sounds :Dictionary = {
	"NotEnoughMinerals":"res://Art/SFX/NotEnoughMineralsSFX.mp3",
	"GameOver":"res://Art/SFX/GameOver.mp3",
	"Victory":"res://Art/SFX/Victory.mp3"
} 
@export_category('Stats:')
@export var count:bool = true

@export var	Stone: int = 0
@onready var tile_map = $TileMap
var player:CharacterBody2D 
var item: Area2D
var time:float = 0
var gameWon:bool = false
#UI
@onready var game_over_texture_rect = $CanvasLayer/Menus/CenterContainer/GameOverTextureRect
@onready var resources : Label = $CanvasLayer/Resources
@onready var timerText = $CanvasLayer/Timer
@onready var pause_center_container = $CanvasLayer/PauseCenterContainer
@onready var game_won_panel = $CanvasLayer/Menus/GameWonPanel
@onready var main_menu_button = $CanvasLayer/Menus/MainMenuButton
@onready var not_enough_minerals_label = $CanvasLayer/NotEnoughMineralsLabel
@onready var tool_tip = $CanvasLayer/ToolTip
@onready var audio_stream_player = $AudioStreamPlayer


@export var minerals = {"stone": 0, "iron": 0, "gold": 0}

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node('Player')
	print("from levelmanager")
	item = player.get_node("Area2D")
	item.map = tile_map
	item.AddMineral.connect(OnAddMineral)
	item.Mined.connect(OnMined)
	player.gameOver.connect(GameOver)
	$CanvasLayer/PauseCenterContainer/Panel/MarginContainer/PauseButton2.connect("pressed",_on_pause_button_pressed)
	# TEMPORARY FIXES HERE:
	
	pass # Replace with function body.
func OnAddMineral(amount):
	minerals['stone'] += amount
	resources.text = "Stone: " + str(minerals['stone'])
func OnRemoveMineral(amount):
	minerals['stone'] -= amount
	resources.text = "Stone: " + str(minerals['stone'])
func OnMined():
	tile_map.removeShadow(tile_map.local_to_map( player.position))
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(count):
		time += delta * int(! get_tree().paused)
	
	var sec = int(time)%60
	var min = int (time/60)
	timerText.text =  str(min) +":"+ str(sec).pad_zeros(2)
	if(min >= 1 && sec >= 30):
		WinGame()
		
func CheckMinerals(minReq:Dictionary):
	for mineral in minReq.keys():
		if minReq[mineral] > minerals[mineral]:
			print("not enough minerals!")
			NotEnoughMineralsMessage(minReq)
			return false
	for mineral in minReq.keys():
		OnRemoveMineral(minReq[mineral])
		return true
func _on_barrier_button_pressed():
	#dprint("yee")
	item.EnterBuildMode()
	pass # Replace with function body
	
func WinGame():
	if(gameWon):
		return
	gameWon = true
	print("you win!")
	get_tree().paused = true
	
	$'CanvasLayer/Menus'.visible = true
	game_won_panel.visible = true
	
	audio_stream_player.stream = load(sounds['Victory'])
	audio_stream_player.play()
	await get_tree().create_timer(1).timeout
	var tween = create_tween()
	main_menu_button.visible = true
	tween.parallel().tween_property(main_menu_button,"modulate",Color(1, 1, 1, 1), 1)
	
	pass
func GameOver():
	print("GameOver")
	get_tree().paused = true
	$'CanvasLayer/Menus'.visible = true
	$'CanvasLayer/Menus/CenterContainer/GameOverTextureRect'.visible = true
	audio_stream_player.stream = load(sounds['GameOver'])
	audio_stream_player.play()
	
	var tween = create_tween()
	tween.tween_property(game_over_texture_rect,"custom_minimum_size",Vector2(1500,1000),1)
	main_menu_button.visible = true
	$'Player/Sprite2D'.frame = 54
	await get_tree().create_timer(1).timeout
	var tween2 = create_tween()
	tween2.tween_property(main_menu_button,"modulate",Color(1, 1, 1, 1), 1)
	await get_tree().create_timer(3).timeout
	tween2.tween_callback(func(): $'Player/Sprite2D'.frame = 55)
	
	

func _on_pause_button_pressed():
	print("Paused Game")
	get_tree().paused = !get_tree().paused
	pause_center_container.visible = get_tree().paused
	pass # Replace with function body.


func _on_main_menu_button_pressed():
	get_tree().change_scene_to_file("res://Scenes/MainMenu.tscn")
	get_tree().paused = false
	pass # Replace with function body.

func NotEnoughMineralsMessage(min:Dictionary):
	var text = 'Not Enough Minerals! You need:\n'
	for m in min.keys():
		var t:String = m + ": " +str(min[m])
		text += t + " "
	
	not_enough_minerals_label.visible = true
	not_enough_minerals_label.text = text
	
	audio_stream_player.stream = load(sounds['NotEnoughMinerals'])
	audio_stream_player.play()
	await get_tree().create_timer(1).timeout
	not_enough_minerals_label.visible = false
	pass



func _on_barrier_button_mouse_entered():
	tool_tip.text = "Cost: 15 Stone"
	
	pass # Replace with function body.


func _on_barrier_button_mouse_exited():
	tool_tip.text = "Cost:"
	pass # Replace with function body.
