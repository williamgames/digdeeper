extends Area2D

signal AddMineral(amount : int)
signal Mined()

@onready var player:CharacterBody2D = $".."
@onready var pickaxe = $Pickaxe
@onready var barrier = $Barrier
@onready var levelManager = get_node('/root/Node2D')


var map: TileMap

var touchingWall:bool = false
var building:bool = false
var mining:bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
#	self.connect("Mined", levelManager.get_node("TileMap").test)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	await get_tree().create_timer(1)
	aim()
	pass
	
func aim():
	var direction = (player.global_position.angle_to_point(get_global_mouse_position()))
	var c = cos(direction)
	var s = sin(direction)
	position.x =  c/(abs(c-s)+abs(c+s)) * 30
	position.y =  s/(abs(c-s)+abs(c+s)) * 30
	
#	if(building):
#		var cell = map.local_to_map(global_position)
#		global_position =  map.map_to_local(cell)
	
	if (player.mouse_left_down):
		if(building):
#			print("placed")
			Build()
		elif (!mining):
			Mine()
			
			
func EnterBuildMode():

	if not levelManager.CheckMinerals({'stone':15}):
		return
	pickaxe.visible = false
	barrier.visible = true
	building = true
	
func ExitBuildMode():
	pickaxe.visible = true
	barrier.visible = false
	building = false
	
	
func Build():
	if(!touchingWall):
		ExitBuildMode()
#		print(global_position)
		var cell = map.local_to_map(global_position)
		if(map.get_cell_tile_data(0,cell) != null):
			map.AddBarrier(cell)
func Mine():
	if(!touchingWall):
		return
	var oldSpeed = player.speed
	player.speed = 0
	mining = true
	var cell = map.local_to_map(global_position)
	if(map.get_cell_tile_data(3,cell) != null):
		await get_tree().create_timer(player.mineSpeed/2).timeout
		
		map.AttackWall(cell)
#		map.RemoveWall(cell)

		emit_signal("AddMineral",1)
		emit_signal("Mined")

		await get_tree().create_timer(player.mineSpeed/2).timeout
		map.astar_grid.set_point_solid(cell-map.grid_offset, false)
	player.speed = oldSpeed
	mining = false

#func removeWall(cell : Vector2i):
#	map.erase_cell(3,cell)
#	map.set_cell(0,cell,1,Vector2i(8,16))
#
#	var sur = []
#	for c in Globals.nearbyCells(cell):
#		if(map.get_cell_tile_data(3,c) != null):
#			map.erase_cell(3,c)
#			sur.append(c)
#
#	map.set_cells_terrain_connect(3,sur,0,0,true)
	
func _on_body_entered(body):
	if(body is TileMap):
		touchingWall = true
		map = body
		var tilemap:TileMap = body
		var cell = tilemap.local_to_map(global_position)
#		var tile_id = tilemap.get_cellv(cell)
		
#		tilemap.set_cell(1,cell,1,Vector2(1,1))
		
	pass # Replace with function body.
func _on_body_exited(body):
	if(body is TileMap):
		touchingWall = false
	pass # Replace with function body.

#func nearbyCells(cell:Vector2i, dist:int = 1):
#	var result = []
#	var tiles = []
#
#	for dx in range(-dist,dist+1):
#		for dy in range(-dist,dist+1):
#			var offset = Vector2i(dx,dy)
#			var newcell = cell + offset
#			tiles.append(newcell)	
#	tiles.erase(Vector2i(0,0))
#
#	result = tiles
#	return result
